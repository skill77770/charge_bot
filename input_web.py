from datetime import datetime
from config import Config
import requests
import time

config = Config()


# Параметры query

def send(url, query_params=''):
    try:
        resp = requests.get(url, params=query_params)
        resp.raise_for_status()
        return resp.text
    except requests.exceptions.RequestException as e:
        return f"ERROR_MON: {e}"


if __name__ == "__main__":
    print("------Start adapter------")
    last_data = None
    while True:
        try:
            current_data = send(config.monitor_url)
            if current_data != last_data:
                current_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                print(f"[{current_time}] UPDATE")
                resp = send(f"http://{config.app_host}:{config.app_port}/input", current_data)
                last_data = current_data
        except requests.exceptions.RequestException as e:
            print(f"ERROR: {e}")

        time.sleep(3)  # Пауза в секундах перед следующей проверкой
