class Config:
    # connect
    token = '1408940901:AAHnpXA_M6e6Vsj0u9jTj4aoY7PA37lqDQQ'
    user_id = 833311494  # id вашего аккаунта
    app_port = 8080
    app_host = '0.0.0.0'

    #web connect
    monitor_url = "https://chargerbot.000webhostapp.com/data" #файл должен содержать только query-параметры


    # датчики
    hybrid = 'k5b'  # гибридная батарея
    voltage = 'k42'  # напряжение батареи
    telephone = 'kff129a'  # АКБ телефона

    # расчёты
    time_full_charge = 180  # время полной зарядки в минутах
    bat_value = 6  # объем батареи в кВт/ч
    voltage_charge = 12.6  # мин. вольтаж для опрееделения зарядки
