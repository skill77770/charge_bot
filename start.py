import subprocess
import time
import signal
import os

scripts = [
    "bot.py",
    "input.py",
    #"input_web.py"
]

processes = []

def start_script(script_name):
    process = subprocess.Popen(["python", script_name])
    processes.append(process)

def stop_all_processes():
    for process in processes:
        try:
            os.kill(process.pid, signal.SIGTERM)
        except ProcessLookupError:
            pass

def main():
    try:
        for script_name in scripts:
            start_script(script_name)

        while True:
            time.sleep(2)
    except KeyboardInterrupt:
        print("Stopping all processes...")
        stop_all_processes()

if __name__ == "__main__":
    main()
