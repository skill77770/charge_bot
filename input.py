from flask import Flask, request
import requests
import json
from config import Config
import time

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False


class Charge:
    def __init__(self):
        self.is_charging = False
        self.is_charged = False

    def stop_charging_mon(self):
        self.is_charging = False

    def start_charging_mon(self):
        self.is_charging = True

    def stop_charged_mon(self):
        self.is_charged = True

    def start_charged_mon(self):
        self.is_charged = False


@app.route('/input', methods=['GET'])
def inp():
    request_data = request.args.to_dict()
    try:
        with open('obd2.json', 'w') as json_file:
            json.dump(request_data, json_file)

        if '"time"' in request_data:
            systime = int(time.time())
            time_resp = int(request_data['"time"'].replace('"', '')[:-4])
            if (systime - time_resp) > 30:
                return 'OK!'
        else:
            if (float(request_data[config.voltage]) > 0) and (float(request_data[config.voltage]) < config.voltage_charge) and (float(request_data[config.hybrid]) < 100):
                if chrg.is_charging:
                    txt = 'Не заряжается'
                    requests.get(f"https://api.telegram.org/bot{config.token}/sendMessage?chat_id={config.user_id}&text={txt}")
                    chrg.stop_charging_mon()

            elif (float(request_data[config.voltage]) > config.voltage_charge) and (not chrg.is_charging):
                txt = 'Пошёл заряд'
                requests.get(f"https://api.telegram.org/bot{config.token}/sendMessage?chat_id={config.user_id}&text={txt}")
                chrg.start_charging_mon()
            else:
                chrg.start_charging_mon()

            if float(request_data[config.hybrid]) >= 99:
                if not chrg.is_charged:
                    txt = 'Заряжен!'
                    requests.get(f"https://api.telegram.org/bot{config.token}/sendMessage?chat_id={config.user_id}&text={txt}")
                    chrg.stop_charged_mon()
            else:
                chrg.start_charged_mon()

        return 'OK!'

    except:
        return 'OK!'


@app.route('/check')
def home():
    return "I'm alive"


if __name__ == '__main__':

    print("------Start input------")
    chrg = Charge()
    config = Config()
    app.run(host=config.app_host, port=config.app_port, debug=False)
