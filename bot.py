from config import Config
import telebot
import json
import datetime

config = Config()
bot = telebot.TeleBot(config.token)  # Токен бота


@bot.message_handler(commands=['start', 'help'])
def help(message):
    bot.send_message(message.chat.id, "Привет " + message.from_user.first_name, reply_markup=keyboard_change())


def keyboard_change():
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=2)
    button1 = telebot.types.KeyboardButton("Status")
    button2 = telebot.types.KeyboardButton("Debug")
    keyboard.add(button1, button2)
    return keyboard


@bot.message_handler(content_types=['text'])
def main(message):
    def percent_conversion(value, old_min, old_max, new_min, new_max):
        old_range = old_max - old_min
        new_range = new_max - new_min
        old_percent = (value - old_min) / old_range
        new_percent = new_min + (old_percent * new_range)
        if value > old_min:
            return new_percent
        else:
            return 0

    def get_info():
        try:
            data = {}
            with open('obd2.json', 'r') as json_file:
                data = json.load(json_file)
            if 'time' in data:
                times = datetime.datetime.fromtimestamp(int(data['time'][:-3])).strftime('%H:%M:%S')
            else:
                times = 'Нет данных с датчика'
            if config.hybrid in data:
                charge = round(percent_conversion(float(data[config.hybrid]), 21, 100, 0, 100), 2)
                time_charge = round(config.time_full_charge - charge * (config.time_full_charge / 100), 0)
                power = round(charge * config.bat_value / 100, 2)
            else:
                charge = 'Нет данных с датчика'
                time_charge = 'Null'
                power = 'Null'

            if config.telephone in data:
                akb = round(float(data[config.telephone]), 0)
            else:
                akb = 'Нет данных с датчика'

            txt = f"Время: {times}\nАКБ: {akb}%\nFord:{charge}% ({power}кВт/ч) ~ {time_charge} мин"

            if config.voltage in data:
                if (float(data[config.voltage]) > 0) and (float(data[config.voltage]) < config.voltage_charge) and (float(data[config.hybrid]) < 100):
                    txt = txt + ' \nНе заряжается!'
                if float(data[config.hybrid]) >= 100:
                    txt = txt + ' \nЗаряжаен!'
            return txt
        except:
            txt = 'DEBUG:\n'
            for key, val in data.items():
                txt = txt + key + str('=' + val + '\n')
            return txt

    def get_debug():
        try:
            with open('obd2.json', 'r') as json_file:
                data = json.load(json_file)
        except:
            txt = 'Нет файла c датчиками'
            return txt
        try:
            txt = 'DEBUG:\n'
            for key, val in data.items():
                txt = txt + key + str('=' + val + '\n')
            return txt
        except:
            txt = 'Ошибка чтения!'
            return txt

    if Config.user_id == message.chat.id:  # проверяем, что пишет именно владелец
        if message.text == "Status":
            bot.send_message(message.chat.id, get_info())

        elif message.text == "Debug":
            bot.send_message(message.chat.id, get_debug())

        else:
            bot.send_message(message.chat.id, 'Invalid input')


if __name__ == '__main__':
    print("------Start bot------")
    bot.polling(none_stop=True)  # запуск бота
